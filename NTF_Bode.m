%  Normalize transfer fonksiyonunu olu�tur.
%                       s^4
%   -----------------------------------------
%   s^4 + 2.613 s^3 + 3.414 s^2 + 2.613 s + 1
NTF=tf([1 0 0 0 0],[1 2.613 3.414 2.613 1]);

% Pay�n k�klerini ekrana yazd�r.
roots([1 0 0 0 0])

% Paydan�n k�klerini ekrana yazd�r.
% s^4 + 2.613*s^3 + 3.414*s^2 + 2.613*s + 1 = (s^2 + 0.765*s + 1)*(s^2 + 1.848*s + 1)
% =(s + 0.383 + i*0.924)*(s + 0.383 - i*0.924)*(s + 0.924 + i*0.383)*(s + 0.924 - i*0.383)
roots([1 2.613 3.414 2.613 1])

% Bode Diyaram�'n� �izdir.
bode(NTF)

% Birim basamak yan�t�n� bul.
step(NTF)

% Kutup s�f�r diyagram�n� �izdir.
pzmap(NTF)